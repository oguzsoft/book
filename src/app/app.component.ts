import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'book';

  items:MenuItem[];

  ngOnInit(){
    this.items = [
      {
        label: 'Anasayfa',
        routerLink:"/"
      },

      {
      label: 'Kitaplar',
      items: [
        {
          label:'Kitapları Görüntüle',
          routerLink:"/kitaplar"
        },
        {
          label:'Kitap Ekle',
          routerLink:"/kitapEkle"
        }
      ]
    }
  
     
    ];

  }
}
