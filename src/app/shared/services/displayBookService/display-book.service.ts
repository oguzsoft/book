import { Injectable } from '@angular/core';
import { Book } from '../../interfaces/book'
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DisplayBookService {

  constructor(private http:Http) { 

  }

  getBooks():Observable<Book[]>{
    return this.http.get("http://localhost:3000/books")
    .pipe(map((res: Response) => res.json()));
   }

}
