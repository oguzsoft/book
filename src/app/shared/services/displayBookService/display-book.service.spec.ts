import { TestBed } from '@angular/core/testing';

import { DisplayBookService } from './display-book.service';

describe('DisplayBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisplayBookService = TestBed.get(DisplayBookService);
    expect(service).toBeTruthy();
  });
});
