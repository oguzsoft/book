import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Book } from '../../interfaces/book'
@Injectable({
  providedIn: 'root'
})
export class AddBookService {

  constructor(private http:HttpClient) { }

  addBook(kitapAdi:string):Observable<Book>{
    return this.http.post<Book>("http://localhost:3000/books", {
      "title": kitapAdi
    });
  }
}
