import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router} from '@angular/router';
import { AppComponent } from './app.component';
import { DisplayBookComponent } from './components/book/display-book/display-book.component';
import { AddBookComponent } from './components/book/add-book/add-book.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { HomeComponent } from './components/home/home/home.component';
import { AppRoutingModule } from './app-routing.module'
import {TableModule} from 'primeng/table';
import {PanelMenuModule} from 'primeng/panelmenu';
import {TabMenuModule} from 'primeng/tabmenu';
import { HttpClientModule } from '@angular/common/http'
import { HttpModule } from '@angular/http'
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
@NgModule({
  declarations: [
    AppComponent,
    DisplayBookComponent,
    AddBookComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TableModule,
    PanelMenuModule,
    TabMenuModule,
    HttpClientModule,
    HttpModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
