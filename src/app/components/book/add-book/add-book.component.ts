import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AddBookService } from '../../../shared/services/addBookService/add-book.service'
@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  kitapAdi:string
  testForm:FormGroup;
  constructor(private addBookService:AddBookService) { }

  ngOnInit() {
    this.createForm()
  }

  createForm(){
    this.testForm = new FormGroup({
      kitapAdi:new FormControl("")
    });
  }

  get form(){
    return this.testForm.controls;
  }

  saveBook(){

    this.addBookService.addBook(this.form.kitapAdi.value)
    .subscribe();
    console.log(this.form.kitapAdi.value);
  }

  onSubmit(){

  }
}
