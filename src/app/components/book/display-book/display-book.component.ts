import { Component, OnInit } from '@angular/core';
import { DisplayBookService } from '../../../shared/services/displayBookService/display-book.service'
import { Book } from '../../../shared/interfaces/book'
@Component({
  selector: 'app-display-book',
  templateUrl: './display-book.component.html',
  styleUrls: ['./display-book.component.css'],
  providers:[DisplayBookService]
})
export class DisplayBookComponent implements OnInit {
  books:Book[];
  
  constructor(private displayBookService:DisplayBookService) { }

  ngOnInit() {
    this.getBook()
  }

  getBook(){
    this.displayBookService.getBooks()
    .subscribe(p=>{
      this.books = p
    });
  }
}
