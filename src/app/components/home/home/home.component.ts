import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  items:MenuItem[];
  constructor() { }

  ngOnInit() {
    this.items = [{
      label: 'Kitaplar',
      items: [
        {
          label:'Kitapları Görüntüle',
          routerLink:"/kitaplar"
        },
        {
          label:'Kitap Ekle',
          routerLink:"/kitapEkle"
        }
      ]
    }
  
     
    ];
  }

}
