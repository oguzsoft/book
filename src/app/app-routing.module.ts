import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http'
import { AppComponent } from './app.component';
import { RouterModule, Routes, Router} from '@angular/router';
import { AddBookComponent } from './components/book/add-book/add-book.component';
import { DisplayBookComponent } from './components/book/display-book/display-book.component';
import { HomeComponent} from './components/home/home/home.component'

export const appRoutes: Routes = [
    {
      path:'',
      component:HomeComponent
    },
    {
        path:'kitapEkle',
        component:AddBookComponent
    },
    {
        path:'kitaplar',
        component:DisplayBookComponent
    }
  ]

@NgModule({
    
    imports:[
        RouterModule.forRoot(appRoutes)

    ],
    exports:[RouterModule]
})
export class AppRoutingModule{

}